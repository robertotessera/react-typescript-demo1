import React from 'react';
import { isLogged } from './authentication.service';

interface IfLoggedProps {
  children: React.ReactNode
}

export const IfLogged: React.FC<IfLoggedProps> = (props) => {
  return isLogged() ? <>{props.children}</> :  null;
};
